**Android programming assignment**

- Application uses Retrofit to access api's along with RxJava to access 
API's.

- Have used Glide for downloading images. Initially had chosen Picasso 
but as the API seems to be providing high-res images in response, 
downscaling of images seemed to be needed which is provided by glide by 
default.


What all would I change to productize 

- The Api doesn't provide a valid json response for failure, thus 
failure cases might not work properly.

- Would additionally add crashlytics instead of the FakeCrashLibrary.

- Additionally would love to do caching.


