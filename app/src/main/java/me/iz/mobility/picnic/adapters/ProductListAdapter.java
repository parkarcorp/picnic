/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 4/17/16 8:29 PM
 *  @modified 4/17/16 2:49 AM
 */

package me.iz.mobility.picnic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.iz.mobility.picnic.R;
import me.iz.mobility.picnic.models.ProductBean;

public class ProductListAdapter extends BaseAdapter {

    private Context mContext;
    private List<ProductBean> productList;
    private List<ProductBean> mOriginalValues;

    private LayoutInflater mInflater;

    public ProductListAdapter(Context mContext, List<ProductBean> productList) {
        this.mContext = mContext;
        this.productList = productList;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public ProductBean getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = mInflater.inflate(R.layout.grid_view_item_product, parent, false);
            holder = new ViewHolder(convertView);

        }

        final ProductBean productInfo = getItem(position);
        holder.tvProductName.setText(productInfo.getName());
        holder.tvProductPrice.setText(productInfo.getPrice()+"");

        final String productImg = productInfo.getImage();
        if (productImg != null && !productImg.equals("")) {
            Glide.with(mContext).load(productImg).
                    placeholder(R.mipmap.ic_launcher).into(holder.ivProduct);
        } else {
            holder.ivProduct.setImageResource(R.mipmap.ic_launcher);
        }

        convertView.setTag(holder);
        return convertView;
    }

    static class ViewHolder {

        @Bind(R.id.tvProductName)
        TextView tvProductName;
        @Bind(R.id.tvPrdPrice)
        TextView tvProductPrice;
        @Bind(R.id.ivProduct)
        ImageView ivProduct;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {

                productList = (ArrayList<ProductBean>) results.values; // has
                // the
                // filtered
                // values
                notifyDataSetChanged(); // notifies the data with new filtered
                // values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults(); // Holds the
                // results of a
                // filtering
                // operation in
                // values
                List<ProductBean> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(
                            productList); // saves the original data in
                    // mOriginalValues
                }

                /********
                 *
                 * If constraint(CharSequence that is received) is null returns
                 * the mOriginalValues(Original) values else does the Filtering
                 * and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {
                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        ProductBean tempBean = mOriginalValues.get(i);
                        final String productName = tempBean.getName().toLowerCase();
                        if (productName.contains(constraint.toString().toLowerCase())) {
                            FilteredArrList.add(tempBean);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

}