/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 4/17/16 6:36 PM
 *  @modified 4/17/16 6:26 PM
 */

package me.iz.mobility.picnic.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;

import butterknife.Bind;
import me.iz.mobility.picnic.R;
import me.iz.mobility.picnic.adapters.ProductListAdapter;
import me.iz.mobility.picnic.controller.BaseController;
import me.iz.mobility.picnic.controller.ProductListController;
import me.iz.mobility.picnic.models.ProductBean;
import me.iz.mobility.picnic.models.ProductList;
import me.iz.mobility.picnic.utils.AppConstants;
import me.iz.mobility.picnic.utils.BaseActivity;
import timber.log.Timber;
import tr.xip.errorview.ErrorView;

public final class ProductsActivity extends BaseActivity implements BaseController.ApiResponseListener {

    @Bind(R.id.gvProductList)
    GridView gvProductList;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Bind(R.id.evError)
    ErrorView evError;

    private ProductListAdapter mAdapter;

    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        init();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_products, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        setupSearchView(searchItem);

        return true;
    }

    public void setupSearchView(MenuItem searchItem) {

        mSearchView.setIconifiedByDefault(true);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String arg0) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {

                Timber.d("Searching %s",arg0);
                if (mAdapter != null)
                    mAdapter.getFilter().filter(arg0);
                return true;
            }
        });
    }

    @Override
    protected String getScreenName() {
        return getString(R.string.products);
    }


    private void init() {

        progressBar.setVisibility(View.VISIBLE);
        gvProductList.setVisibility(View.GONE);
        evError.setVisibility(View.GONE);

        apiController = new ProductListController(this, null);
        apiController.start();

    }

    @Override
    public void onSuccess(Object obj) {

        progressBar.setVisibility(View.GONE);
        gvProductList.setVisibility(View.VISIBLE);
        evError.setVisibility(View.GONE);

        ProductList productBeanList = (ProductList) obj;

        mAdapter = new ProductListAdapter(this,productBeanList.getProductList());
        gvProductList.setAdapter(mAdapter);
        gvProductList.setOnItemClickListener((parent, view, position, id) -> {
            ProductBean mInfo = (ProductBean) parent.getItemAtPosition(position);
            Timber.d("Selected product %s", mInfo.toString());
            Intent intent = new Intent(ProductsActivity.this, ProductDetailActivity.class);
            intent.putExtra(AppConstants.PRODUCT_ID,mInfo.getProductId());
            startActivity(intent);

        });

    }

    @Override
    public void onFailure(String errorMessage) {

        progressBar.setVisibility(View.GONE);
        gvProductList.setVisibility(View.GONE);
        evError.setVisibility(View.VISIBLE);
        evError.setErrorTitle(errorMessage);
        evError.setOnRetryListener(this::init);

    }
}
