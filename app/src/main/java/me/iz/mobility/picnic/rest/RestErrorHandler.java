/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 4/17/16 6:38 PM
 *  @modified 4/17/16 2:49 AM
 */

package me.iz.mobility.picnic.rest;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

public class RestErrorHandler {


    public static void handleError(Throwable throwable) throws UnauthorizedException {
        if (throwable instanceof HttpException) {

            Response<?> response = ((HttpException) throwable).response();

            Converter<ResponseBody, RestError> converter =
                    RestConnection.getRestAdapter()
                            .responseBodyConverter(RestError.class, new Annotation[0]);
            try {
                RestError restError = converter.convert(response.errorBody());
                throw new UnauthorizedException(response.code(), restError.getErrorMessage());

            } catch (IOException e) {
                e.printStackTrace();
                throw new UnauthorizedException(((HttpException) throwable).code(), throwable.getMessage());
            }
        } else

            throw new UnauthorizedException(666, throwable.getMessage());
    }
}