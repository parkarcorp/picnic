/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 4/17/16 7:55 PM
 *  @modified 4/17/16 7:55 PM
 */

package me.iz.mobility.picnic.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import me.iz.mobility.picnic.R;
import me.iz.mobility.picnic.controller.BaseController.ApiResponseListener;
import me.iz.mobility.picnic.controller.ProductDetailController;
import me.iz.mobility.picnic.models.ProductDetail;
import me.iz.mobility.picnic.utils.AppConstants;
import me.iz.mobility.picnic.utils.BaseActivity;
import tr.xip.errorview.ErrorView;

public class ProductDetailActivity extends BaseActivity implements ApiResponseListener {

    @Bind(R.id.rlProductDetailContainer)
    RelativeLayout llProductDetailContainer;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Bind(R.id.evError)
    ErrorView evError;

    @Bind(R.id.tvProductName)
    TextView tvProductName;

    @Bind(R.id.tvProductDesc)
    TextView tvProductDesc;

    @Bind(R.id.tvProductPrice)
    TextView tvProductPrice;

    @Bind(R.id.ivProduct)
    ImageView ivProduct;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        String productId = getIntent().getStringExtra(AppConstants.PRODUCT_ID);
        if(productId == null) {
            onFailure(getString(R.string.no_product_id_fount));
            return;
        }

        init(productId);

    }


    private void init(String productId) {

        progressBar.setVisibility(View.VISIBLE);
        llProductDetailContainer.setVisibility(View.GONE);
        evError.setVisibility(View.GONE);

        apiController = new ProductDetailController(this, productId);
        apiController.start();

    }

    @Override
    protected String getScreenName() {
        return getString(R.string.product_detail);

    }

    @Override
    public void onSuccess(Object obj) {

        progressBar.setVisibility(View.GONE);
        llProductDetailContainer.setVisibility(View.VISIBLE);
        evError.setVisibility(View.GONE);

        ProductDetail productDetail = (ProductDetail) obj;

        tvProductName.setText(productDetail.getName());
        tvProductDesc.setText(productDetail.getDescription());
        tvProductPrice.setText(productDetail.getPrice()+"");
        Glide.with(this).load(productDetail.getImage()).
                placeholder(R.mipmap.ic_launcher).into(ivProduct);
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.setVisibility(View.GONE);
        llProductDetailContainer.setVisibility(View.GONE);
        evError.setVisibility(View.VISIBLE);
        evError.setErrorTitle(errorMessage);
        evError.showRetryButton(false);

    }
}
