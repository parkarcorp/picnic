/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 4/17/16 6:38 PM
 *  @modified 4/17/16 2:49 AM
 */

package me.iz.mobility.picnic.controller;


import java.io.Closeable;

import me.iz.mobility.picnic.utils.RxUtils;
import rx.subscriptions.CompositeSubscription;

/**
 * @author ibasit
 */
public abstract class BaseController implements Closeable {

    private final String TAG = getClass().getSimpleName();

    protected CompositeSubscription subscriptions = new CompositeSubscription();

    protected ApiResponseListener mCallback;

    protected BaseController(ApiResponseListener mCallback) {
        this.mCallback = mCallback;
    }

    @Override
    public void close(){
        RxUtils.unsubscribeIfNotNull(subscriptions);
    }

    public void onPause() {
        close();
    }

    public void onResume() {
        subscriptions = RxUtils.getNewCompositeSubIfUnsubscribed(subscriptions);
    }


    public interface ApiResponseListener {
        void onSuccess(Object obj);
        void onFailure(String errorMessage);
    }

    protected abstract void processApi();

    public abstract void start();
}
