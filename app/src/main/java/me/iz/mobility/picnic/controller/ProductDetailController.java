/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 4/17/16 7:53 PM
 *  @modified 4/17/16 7:07 PM
 */

package me.iz.mobility.picnic.controller;


import com.fernandocejas.frodo.annotation.RxLogObservable;

import me.iz.mobility.picnic.models.ProductDetail;
import me.iz.mobility.picnic.rest.UnauthorizedException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static me.iz.mobility.picnic.rest.RestConnection.createPicnicService;
import static me.iz.mobility.picnic.rest.RestErrorHandler.handleError;


/**
 * @author ibasit
 */
public class ProductDetailController extends BaseController {

    private final String TAG = getClass().getSimpleName();

    String productId;

    public ProductDetailController(ApiResponseListener mListener, Object input) {
        super(mListener);

        productId = (String) input;
    }

    @Override
    protected void processApi() {

        subscriptions.add(getProductDetail() // get product detail
                .subscribeOn(Schedulers.io()) // do processing on the io thread
                .observeOn(AndroidSchedulers.mainThread()) // get response on main thread
                .subscribe(productDetail -> {
                    mCallback.onSuccess(productDetail); // post response to ui
                }, throwable ->
                {
                    try {
                        handleError(throwable);
                    } catch (UnauthorizedException throwable1) {
                        Timber.e(throwable.getMessage());
                        mCallback.onFailure(throwable1.getMessage());
                    }
                }));
    }

    @Override
    public void start() {
        processApi();
    }


    @RxLogObservable
    private Observable<ProductDetail> getProductDetail() {
        return createPicnicService().getProductDetail(productId);
    }
}
