/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 4/17/16 8:49 PM
 *  @modified 4/17/16 8:49 PM
 */

package me.iz.mobility.picnic.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import java.text.DecimalFormat;

import timber.log.Timber;

/**
 * @author ibasit
 */
public class CurrencyTextView extends TextView {

    private final String TAG = getClass().getSimpleName();

    public CurrencyTextView(Context context) {
        super(context);
    }

    public CurrencyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CurrencyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void setText(CharSequence text, BufferType type) {

        String formattedString = text.toString();

        if(formattedString.isEmpty())
            return;

        try {
            DecimalFormat decimalFormat = new DecimalFormat("€ ###,###,###,###.##");
            int price = Integer.parseInt(text.toString());
            float formattedPrice = (float) price/100;
            formattedString = decimalFormat.format(formattedPrice);

        }catch (Exception e){
            Timber.e(e.getLocalizedMessage());
        }
        super.setText(formattedString, type);
    }
}
